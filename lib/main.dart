import 'package:flutter/material.dart';
import 'package:spannable_grid/spannable_grid.dart';
import './grid_json.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SpannableGrid Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        accentColor: Colors.amber,
      ),
      home: MyHomePage(title: 'Calculator App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SpannableGrid(
        columns: 4,
        rows: 6,
        cells: _getCells(),
        spacing: 0,
        onCellChanged: (cell) {
          print('Cell ${cell.id} changed');
        },
//              showGrid: true,
//              emptyCellView: Container(color: Colors.amberAccent,),
//              editingOnLongPress: false,
      ),
    );
  }

  List<SpannableGridCellData> _getCells() {
    List<SpannableGridCellData> result = List();
    gridConfig.forEach((cell) => {
          result.add(SpannableGridCellData(
              id: cell["cellId"],
              column: cell["column"],
              row: cell["row"],
              columnSpan: cell["colspan"],
              rowSpan: cell["rowspan"],
              child: Container(
                color: Colors.white,
                child: Center(
                  child: cell["widget"],
                ),
              )))
        });

    return result;
  }
}
