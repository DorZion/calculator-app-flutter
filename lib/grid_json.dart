import 'package:flutter/material.dart';
import 'clacultor_icons.dart';

final gridConfig = [
  {
    "cellId": 1,
    "column": 1,
    "row": 1,
    "rowspan": 2,
    "colspan": 4,
    "text": "text_filed",
    "widget": TextField(
        obscureText: true,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: '0',
        ))
  },
  {
    "cellId": 2,
    "column": 1,
    "row": 3,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 1",
    "widget": Tab(icon: Icon(CalculatorIcons.seven))
  },
  {
    "cellId": 3,
    "column": 2,
    "row": 3,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 2",
    "widget": Tab(icon: Icon(CalculatorIcons.eight))
  },
  {
    "cellId": 4,
    "column": 3,
    "row": 3,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 3",
    "widget": Tab(icon: Icon(CalculatorIcons.nine))
  },
  {
    "cellId": 5,
    "column": 4,
    "row": 3,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 4",
    "widget": Tab(icon: Icon(CalculatorIcons.divide_outline))
  },
  {
    "cellId": 6,
    "column": 1,
    "row": 4,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 5",
    "widget": Tab(icon: Icon(CalculatorIcons.four))
  },
  {
    "cellId": 7,
    "column": 2,
    "row": 4,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 6",
    "widget": Tab(icon: Icon(CalculatorIcons.five))
  },
  {
    "cellId": 8,
    "column": 3,
    "row": 4,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 7",
    "widget": Tab(icon: Icon(CalculatorIcons.six))
  },
  {
    "cellId": 9,
    "column": 4,
    "row": 4,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 8",
    "widget": Tab(icon: Icon(CalculatorIcons.cancel_outline))
  },
  {
    "cellId": 10,
    "column": 1,
    "row": 5,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 9",
    "widget": Tab(icon: Icon(CalculatorIcons.one))
  },
  {
    "cellId": 11,
    "column": 2,
    "row": 5,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 10",
    "widget": Tab(icon: Icon(CalculatorIcons.two))
  },
  {
    "cellId": 12,
    "column": 3,
    "row": 5,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 11",
    "widget": Tab(icon: Icon(CalculatorIcons.three))
  },
  {
    "cellId": 13,
    "column": 4,
    "row": 5,
    "rowspan": 1,
    "colspan": 1,
    "widget": Tab(icon: Icon(CalculatorIcons.plus_outline))
  },
  {
    "cellId": 14,
    "column": 1,
    "row": 6,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 13",
    "widget": Tab(icon: Icon(CalculatorIcons.eq_outline))
  },
  {
    "cellId": 15,
    "column": 2,
    "row": 6,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 14",
    "widget": Tab(icon: Icon(CalculatorIcons.zero))
  },
  {
    "cellId": 16,
    "column": 3,
    "row": 6,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 15",
    "widget": Text("CLEAR")
  },
  {
    "cellId": 17,
    "column": 4,
    "row": 6,
    "rowspan": 1,
    "colspan": 1,
    "text": "Cell 16",
    "widget": Tab(icon: Icon(CalculatorIcons.minus_outline))
  }
];
